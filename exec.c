#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Usage: %s <source file> [source file] ...\n", argv[0]);
        return 1;
    }

    // Compile the source files
    pid_t pid = fork();
    if (pid == 0) {
        // Child process - execute the compiler
        char* compiler_args[] = {"gcc", "-o", "a.out", NULL};
        execvp("gcc", compiler_args);
        perror("execvp");
        return 1;
    } else if (pid > 0) {
        // Parent process - wait for the child to finish
        int status;
        waitpid(pid, &status, 0);
        if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
            printf("Compilation failed\n");
            return 1;
        }

        // Run the compiled program
        pid = fork();
        if (pid == 0) {
            // Child process - execute the program
            char* program_args[] = {"./a.out", NULL};
            execvp("./a.out", program_args);
            perror("execvp");
            return 1;
        } else if (pid > 0) {
            // Parent process - wait for the child to finish
            waitpid(pid, &status, 0);
            if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
                printf("Program execution failed\n");
                return 1;
            }
        } else {
            perror("fork");
            return 1;
        }
    } else {
        perror("fork");
        return 1;
    }

    return 0;
}
