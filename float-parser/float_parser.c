#include<stdio.h>
#include<string.h>

typedef union {
    float f;
    struct {
        unsigned int mantissa : 23;
        unsigned int exponent : 8;
        unsigned int sign : 1;
    } raw;
} float754;

// convert int to binary representation
void convertBinary(int n, int size_bits) {
    for (int k = size_bits - 1; k >= 0; k--) 
        if ((n >> k) & 1)
            printf("1");
        else
            printf("0");
}


// convert float to IEEE754
void convert(float754 var) {
    printf("%d|", var.raw.sign);
	convertBinary(var.raw.exponent, 8);
	printf("|");
	convertBinary(var.raw.mantissa, 23);
	printf("\n");;
}

// driver code
int main()
{
    float754 var;
    scanf("%f", &var.f);
    convert(var);
    return 0;
}
