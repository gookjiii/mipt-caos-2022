# MIPT - CAOS - 2022

This code first checks if the user provided a file name as an argument, then it opens the file in binary mode. Next, it gets the file size, and checks if it is a multiple of the struct size, if not it prints an error message and exits. It also calculates the number of structs in the file.

Then it creates a set to store the address of next pointer, this will be used to check for cycles. Next, it reads the file using a while loop and within the loop it checks the next pointer, if it is pointing to somewhere out of the file or to the middle of another structure, it prints an error message and exits. Also, it checks for cycles, if it finds any, it prints an error message and exits
