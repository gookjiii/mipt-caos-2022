.model tiny
.data

Storage dw 0, 0

MsgOne db 'Welcome to the joke generator!', 0
MsgTwo db 'Would you like to hear a joke?', 0
MsgThree db 'I'll take that as a yes', 0
MsgFour db 'Why was the math book sad?', 0
MsgFive db 'Because it had too many problems', 0

.code

COLOR equ 0Ah
WHITE equ 0Fh

org 100h

Start:
    call ClrScr

    mov ax, 0B800h
    mov es, ax

;======================Introduction=======================================

    mov di, 80*6*2+28*2
    mov bx, offset MsgOne
    call PrintLine

    call LongDelay

    mov di, 80*8*2+28*2
    mov bx, offset MsgTwo
    call PrintLine

    call LongDelay

;=====================Generator=launch====================================

    call ClrScr

    mov di, 80*6*2+28*2
    mov bx, offset MsgThree
    call PrintLine

    call LongDelay

;=====================Print=a=frame========================================

    mov di, 80*8*2+8*2
    mov cx, 60
    mov bx, 7
    call PrintFrame

    call LongDelay

;======================Print=a=joke=========================================

    mov di, 80*10*2+13*2
    mov bx, offset MsgFour
    call PrintLine

    mov di, 80*12*2+18*2
    mov bx, offset MsgFive
    call PrintLine

    call LongDelay

    ret

;=======================PROCEDURES=========================================

;=======================LONG DELAY=========================================
;==========================================================================
; Expects: none
;
; Returns: none
;
; Destroys: ax, cx, dx
;==========================================================================
LongDelay proc
    mov ax, 8600h
    mov cx, 000Ah
    mov dx, 0000h
    int 15h

    ret
endp LongDelay

;===================================================
; Expects: none
;
; Returns: none
;
; Destroys: ax
;===================================================
Delay proc
    mov ax, 8600h

    mov word ptr [Storage], cx
    mov word ptr [Storage+2], dx
    mov cx, 0000h
    mov dx, 8000h
    int 15h

    mov cx, word ptr [Storage]
    mov dx, word ptr [Storage+2]
    
    ret
endp Delay

;===================================================
; Expects: bx - offset to string
;
; Returns: none
;
; Destroys: ax
;===================================================

PrintLine proc
    mov al, [bx]
    mov ah, COLOR

    cld

Symbol:
    cmp al, 0
    je FinalPrt
    
    stosw

    inc bx
    
    call Delay

    mov al, [bx]
    mov ah, COLOR

    jmp Symbol

FinalPrt:
