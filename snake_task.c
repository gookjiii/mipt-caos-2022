#include <stdio.h>
#include <stdint.h>

struct list_member {
    int value;
    uint32_t next;
};

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Error: Please provide a file name\n");
        return 1;
    }

    // Open the file
    FILE* fp = fopen(argv[1], "rb");
    if (!fp) {
        printf("Error: Failed to open file\n");
        return 1;
    }

    // Get the file size
    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    rewind(fp);

    // Check if the file size is a multiple of the struct size
    if (file_size % sizeof(struct list_member) != 0) {
        printf("Error: Invalid file size\n");
        fclose(fp);
        return 1;
    }

    // Calculate the number of structs in the file
    int num_structs = file_size / sizeof(struct list_member);

    // Create a set to store the address of next pointer 
    int set[num_structs];
    int i;
    for(i=0;i<num_structs;i++)
        set[i] = -1;

    // Read the file
    struct list_member member;
    int struct_count = 0;
    while (fread(&member, sizeof(struct list_member), 1, fp) == 1) {
        if (member.next > file_size) {
            printf("Error: Invalid pointer\n");
            fclose(fp);
            return 1;
        }
        if(set[member.next] != -1){
            printf("Error: File contains cycles\n");
            fclose(fp);
            return 1;
        }
        set[member.next] = 1;
        printf("Value: %d\n", member.value);
        struct_count++;
        if (member.next == 0) {
            if (struct_count != num_structs) {
                printf("Error: Pointer to zero found too early\n");
                fclose(fp);
                return 1;
            }
            break;
        } else {
            fseek(fp, member.next, SEEK_SET);
        }
    }

    fclose(fp);
    return 0;
}
