#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>

void cp_r(const char* src, const char* dst) {
    struct stat st;
    if (stat(src, &st) != 0) {
        perror("stat");
        return;
    }

    if (S_ISDIR(st.st_mode)) {
        DIR* dir;
        struct dirent* ent;

        if ((dir = opendir(src)) != NULL) {
            if (mkdir(dst, 0777) != 0 && errno != EEXIST) {
                perror("mkdir");
                return;
            }

            while ((ent = readdir(dir)) != NULL) {
                char* name = ent->d_name;

                if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0) {
                    continue;
                }

                char* src_path = malloc(strlen(src) + strlen(name) + 2);
                sprintf(src_path, "%s/%s", src, name);

                char* dst_path = malloc(strlen(dst) + strlen(name) + 2);
                sprintf(dst_path, "%s/%s", dst, name);

                cp_r(src_path, dst_path);
                free(src_path);
                free(dst_path);
            }
            closedir(dir);
        }
    } else {
        FILE* src_file = fopen(src, "r");
        FILE* dst_file = fopen(dst, "w");

        if (!src_file) {
            perror("fopen src");
            return;
        }
        if (!dst_file) {
            perror("fopen dst");
            return;
        }

        char buffer[4096];
        size_t bytes;

        while ((bytes = fread(buffer, 1, sizeof(buffer), src_file)) > 0) {
            fwrite(buffer, 1, bytes, dst_file);
        }

        fclose(src_file);
        fclose(dst_file);
    }
}
