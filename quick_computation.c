#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

const int MAX_THREADS = 1024;

int data[100];
int thread_count;

void merge(int a[], int low, int mid, int high) {
    int b[100];
    int i, j, k;

    j = low;
    k = mid + 1;
    for (i = low; j <= mid && k <= high; i++) {
        if (a[j] <= a[k]) {
            b[i] = a[j++];
        } else {
            b[i] = a[k++];
        }
    }

    if (j > mid) {
        for (; k <= high; k++) {
            b[i++] = a[k];
        }
    } else {
        for (; j <= mid; j++) {
            b[i++] = a[j];
        }
    }

    for (i = low; i <= high; i++) {
        a[i] = b[i];
    }
}

void *merge_sort(void *args) {
    int thread_id = *(int *) args;
    int low = thread_id * (100 / thread_count);
    int high = (thread_id + 1) * (100 / thread_count) - 1;
    int mid = (low + high) / 2;

    if (low < high) {
        int left_thread_id = thread_id * 2;
        int right_thread_id = thread_id * 2 + 1;

        pthread_t left_thread, right_thread;
        int left_thread_args = left_thread_id;
        int right_thread_args = right_thread_id;

        pthread_create(&left_thread, NULL, merge_sort, &left_thread_args);
        pthread_create(&right_thread, NULL, merge_sort, &right_thread_args);

        pthread_join(left_thread, NULL);
        pthread_join(right_thread, NULL);

        merge(data, low, mid, high);
    }

    return NULL;
}

int main(int argc, char *argv[]) {
    thread_count = 2;

    int i;
    for (i = 0; i < 100; i++) {
        data[i] = rand() % 100;
    }

    int thread_id = 0;
    pthread_t sort_thread;
    pthread_create(&sort_thread, NULL, merge_sort, &thread_id);
    pthread_join(sort_thread, NULL);

    for (i = 0; i < 100; i++) {
        printf("%d ", data[i]);
    }

    return 0;
}
