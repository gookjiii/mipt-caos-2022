#include <dirent.h>
#include <sys/stat.h>
#include <stdio.h>
#include <pwd.h>
#include <grp.h>

void ls_l() {
    DIR *dir = opendir(".");
    if (dir == NULL) {
        perror("opendir");
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_name[0] == '.') {
            continue;
        }

        struct stat info;
        if (stat(entry->d_name, &info) == -1) {
            perror("stat");
            continue;
        }

        printf( (S_ISDIR(info.st_mode)) ? "d" : "-");
        printf( (info.st_mode & S_IRUSR) ? "r" : "-");
        printf( (info.st_mode & S_IWUSR) ? "w" : "-");
        printf( (info.st_mode & S_IXUSR) ? "x" : "-");
        printf( (info.st_mode & S_IRGRP) ? "r" : "-");
        printf( (info.st_mode & S_IWGRP) ? "w" : "-");
        printf( (info.st_mode & S_IXGRP) ? "x" : "-");
        printf( (info.st_mode & S_IROTH) ? "r" : "-");
        printf( (info.st_mode & S_IWOTH) ? "w" : "-");
        printf( (info.st_mode & S_IXOTH) ? "x" : "-");
        printf(" %ld", info.st_nlink);
        struct passwd *pw = getpwuid(info.st_uid);
        printf(" %s", pw->pw_name);
        struct group *gr = getgrgid(info.st_gid);
        printf(" %s", gr->gr_name);
        printf(" %ld", info.st_size);
        printf(" %s", entry->d_name);
        printf("\n");
    }

    closedir(dir);
}
